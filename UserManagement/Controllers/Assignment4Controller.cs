﻿using EADF17_Assignment4_MCSF16M027.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EADF17_Assignment4_MCSF16M027.Controllers
{
    public class Assignment4Controller : Controller
    {
        //
        // GET: /Assignment4/
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [ActionName("Login")]
        [HttpPost]
        public ActionResult Login2(User user)
        {
            if (Validation.doesExist(user.txtUserName,user.txtPassword))
            {
                Session["UserName"] = user.txtUserName;
                return Redirect("~/Assignment4/Home");
            }
            else
            {
                ViewBag.Msg = "Invalid";
                ViewBag.UserName = user.txtUserName;
            }
            return View("Login");
        }
        [HttpGet]
        public ActionResult Home()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Assignment4/Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Signout()
        {
            Session["UserName"] = null;
            Session.Abandon();
            return Redirect("~/Assignment4/Login");
        }
        [HttpGet]
        public ActionResult Email()
        {
            return View();
        }
        [ActionName("NewPass")]
        [HttpPost]
        public ActionResult NewPass1()
        {
            String login = Request["txtUser"];
            String email = Request["txtUserName"];
            if (Validation.sendEmail(login, email))
            {
                return View();
            }
            ViewBag.Login = login;
            ViewBag.Email = email;
            ViewBag.Msg = "Invalid";
            return View("Email");
        }

        [HttpPost]
        public ActionResult SignUp()
        {
            string name = Request["txtUserName1"];
            string login = Request["txtUserLogin1"];
            string email = Request["emailtxt"];
            string password = Request["txtPassword1"];
            if (name == null || login == null || email == null || password == null)
            {
                return Redirect("~/Assignment4/Login");
            }
            if (Validation.insert(name, login, password, email))
            {
                Session["UserName"] = name;
                return Redirect("~/Assignment4/Home");
            }
            return Redirect("~/Assignment4/Login");
        }
        [HttpPost]
        public ActionResult GoHome()
        {
            string login = Request["txtUserName"];
            string password = Request["txtUserPassword"];
            string newPassword = Request["txtUserNewPAssword"];
            if (Validation.changePassword(login,password,newPassword))
            {
                Session["UserName"] = login;
                return Redirect("~/Assignment4/Home");
            }
            ViewBag.Msg = "Invalid Login Or Code";
            ViewBag.Login = login;
            ViewBag.Code = password;
            Session["name"] = login;
            return View("NewPass");
        }
        [HttpGet]
        public ActionResult NewPass()
        {
            return View();
        }
	}
}
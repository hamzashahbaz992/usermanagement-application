﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Net.Mail;
using System.Net;

namespace EADF17_Assignment4_MCSF16M027.Models
{
    public class Validation
    {
        public static Boolean doesExist(String login, String password)
        {
            SqlConnection con = new SqlConnection(ConnectionProvider.getConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select * from Users where Login = '" + login + "' and Password = '" + password + "'";
            cmd.Connection = con;
            SqlDataReader rd = cmd.ExecuteReader();
            int count = 0;
            while (rd.Read())
            {
                count++;
            }
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static Boolean isValid(string loginName)
        {
            SqlConnection con = new SqlConnection(ConnectionProvider.getConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select * from Users where Login = '" + loginName + "'";
            cmd.Connection = con;
            SqlDataReader rd = cmd.ExecuteReader();
            int c = 0;
            while (rd.Read())
            {
                c++;
            }
            if (c == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static Boolean insert(String name,String login , String password, String Email)
        {
            SqlConnection conn = new SqlConnection(ConnectionProvider.getConnectionString());
            if (isValid(login) == false)
            {
                return false;
            }
            try
            {
                conn.Open();
                string q = "INSERT INTO Users(Name,Login,Password,Email) VALUES('" + name + "', '" + login + "', '" + password + "', '" + Email + "')";
                SqlCommand cmd = new SqlCommand(q, conn);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static Boolean sendEmail(String login,String email)
        {
            SqlConnection con = new SqlConnection(ConnectionProvider.getConnectionString());
            con.Open();
            string password = "";
            string message = "Your code for loginID ";
            message = message + login + " is ";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select * from Users where Login = '" + login + "' and Email = '" + email + "'";
            cmd.Connection = con;
            SqlDataReader rd = cmd.ExecuteReader();
            int count = 0;
            while (rd.Read())
            {
                password = rd["Password"].ToString();
                count++;
            }
            if (count > 0)
            {
                try
                {
                    message = message + password;
                    MailMessage msg = new MailMessage("testmail5134@gmail.com", email, "Code For Password Recovery", message);
                    msg.IsBodyHtml = true;
                    SmtpClient sc = new SmtpClient("smtp.gmail.com", 587);
                    sc.UseDefaultCredentials = false;
                    NetworkCredential cre = new NetworkCredential("testmail5134@gmail.com", "12345678test");
                    sc.Credentials = cre;
                    sc.EnableSsl = true;
                    sc.Send(msg);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public static Boolean changePassword(String login,String password,String newPassword)
        {
            if (doesExist(login,password))
            {
                SqlConnection con = new SqlConnection(ConnectionProvider.getConnectionString());
                con.Open();
                string q = "UPDATE Users SET Password = '" + newPassword + "' where Login = '" + login + "'";
                SqlCommand cmd = new SqlCommand(q, con);
                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
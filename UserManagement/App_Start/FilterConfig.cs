﻿using System.Web;
using System.Web.Mvc;

namespace EADF17_Assignment4_MCSF16M027
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
